---
title: "About Us"
date: 2018-07-12T18:19:33+06:00
heading : "WE ARE SRUFF AG. AN AWARD-WINNING COMPANY IN ZURICH."
description : "We are specialized in helping our customers developing their forward-thinking skills. And we do this by bringing our customers through each phase of the löpp-process with us."
expertise_title: "Expertise"
expertise_sectors: ["Agile Living", "Agile Shooting", "Agile Löpping", "Agile Working", "Agile Leadership", "Agile Thinking", "Agile Forward-Looking"]
---
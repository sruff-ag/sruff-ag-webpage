---
title: "USE-LESS BAU und ABRISS LLC"
date: 2019-12-23T15:58:10+06:00
type: portfolio
image: "images/projects/project-thumb-one.jpg"
category: ["AGILE WORKING"]
project_images: ["images/projects/project-details-image-one.jpg", "images/projects/project-details-image-two.jpg"]
---

Dieser Kunde hatte Probleme mit Mitarbeitenden, welche die falschen Gebäude abrissen und so hohe Kosten verursachten. Wir haben die Verwendung eines Belohnungs- und Disziplinierungssystems durchgesetzt: Neu werden Mitarbeitende, welche mehr als 3 Gebäude fälschlicherweise beschädigt haben, mit einer gelben Karte verwarnt.

Diese Lösung hat bisher keine Wirkung gezeigt. Die Lösung wurde vor 15 Jahren implementiert
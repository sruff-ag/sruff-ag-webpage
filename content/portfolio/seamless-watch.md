---
title: "WEWATCHYOU Sicherheit & Ordnung is unser täglich Brot AG"
date: 2019-12-23T15:55:44+06:00
type: portfolio
image: "images/projects/project-thumb-three.jpg"
category: ["AGILE LIVING"]
project_images: ["images/projects/project-details-image-one.jpg", "images/projects/project-details-image-two.jpg"]
---

Der Kunde hatte Probleme mit Schlägereien im Büro. Wir haben empfohlen, die gläsernen Bierflaschen durch Plastikflaschen zu ersetzen. Die Kosten nach den Schlägereien wurden neu ebenfalls auf die Mitarbeiter abgewälzt, welche an diesem Tag nicht im Büro vor Ort waren.
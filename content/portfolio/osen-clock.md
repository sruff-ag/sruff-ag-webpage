---
title: "OSEN CLOCK & GLOCKS Inc."
date: 2019-12-23T15:56:43+06:00
type: portfolio
image: "images/projects/project-thumb-two.jpg"
category: ["AGILE LIVING"]
project_images: ["images/projects/project-details-image-one.jpg", "images/projects/project-details-image-two.jpg"]
---

Wir haben dem Kunden empfohlen einige Plakate mit Zitaten zum Thema "Agile Living" in den Büros aufzuhängen.
Die Produktivität der Mitarbeiter hat um 100 % zugenommen.


Zitate:


> WE DONT LIVE, WE WORK


> FAMILY IS NOT A PRIORITY, WORK IS

> DONT CHILL: WORK!

> JUST WORK IT AWAY!
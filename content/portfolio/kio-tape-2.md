---
title: "KIO-TAPE Blumenvasen & Co. KG"
date: 2019-12-23T20:56:42+06:00
type: portfolio
image: "images/projects/project-thumb-four.jpg"
category: ["AGILE LÖPPING"]
project_images: ["images/projects/project-details-image-one.jpg", "images/projects/project-details-image-two.jpg"]
---

Bei diesem Kunden konnten wir leider nicht helfen, deshalb haben wir das Geld mitgenommen und die Arbeit nicht angetreten.

Wir danken für das Vertrauen des Kunden, welchen wir hier verarscht haben.

Wir würden uns sehr über eine positive Rückmeldung auf unserer Webseite freuen

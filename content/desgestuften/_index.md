---
title: "IM NAMEN der Stufung"
date: 2018-07-12T18:19:33+06:00
heading : "Wir stufen ab und weg"
description : "We are specialized in helping our customers developing their forward-thinking skills. And we do this by bringing our customers through each phase of the löpp-process with us."
expertise_title: "Expertise"
expertise_sectors: ["Agile Living", "Agile Shooting", "Agile Löpping", "Agile Working", "Agile Leadership", "Agile Thinking", "Agile Forward-Looking"]
---

## Glossar informativ

<b>Weistbenutze Wörter der SRUFF-AG im übertragenem Sinne</b>

- 1. weggestuft

- 2. abgestuft

- 3. angestuft (auch angeheftet)

- 4. Nasenwasser

- 5. Spinner
# SRUFF AG - Webpage


```
 ____  ____  _   _ _____ _____      _    ____
/ ___||  _ \| | | |  ___|  ___|    / \  / ___|
\___ \| |_) | | | | |_  | |_      / _ \| |  _
 ___) |  _ <| |_| |  _| |  _|    / ___ \ |_| |
|____/|_| \_\\___/|_|   |_|     /_/   \_\____|
```

```
                _                          _
__      _____  | | ___   ___ _ __  _ __   (_)_ __
\ \ /\ / / _ \ | |/ _ \ / _ \ '_ \| '_ \  | | '_ \
 \ V  V /  __/ | | (_) |  __/ |_) | |_) | | | | | |
  \_/\_/ \___| |_|\___/ \___| .__/| .__/  |_|_| |_|
                            |_|   |_|
```

### SRUFF AG - _We löpp in and out for our customers_
<br>

**ORLANDO MAXEN** <br>
Chief Executive Officer & 1. Mann im Betriebe

**SANDO BELMUTZ** <br>
Founder & Administrator GROUPE LÖPP - LÖPP-GROUP  
Chief Operation Officer & Stellvertretender Geschäftsleiter (2. Mann im Betriebe)

**CONTI MAXEN** <br>
Chief Security Officer, Ordnungs- und Domänenmann

**LENOT LÖPPIGER** <br>
Chief Handling Officer & Facility Manager General


<br><br><br>

_a project of the GROUPE LÖPP - LÖPP-GROUP_